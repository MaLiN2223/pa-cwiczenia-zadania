﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Zadanie4
{
    public class Composition<T> : IEnumerable<Func<T, T>>
    {
        private readonly LinkedList<Func<T, T>> _list = new LinkedList<Func<T, T>>();
        public Func<T, T> _function;

        public Composition(params Func<T, T>[] functions)
        {
            foreach (var func in functions)
            {
                Add(func);
            }
        }

        public Composition(params Composition<T>[] compositions)
        {
            foreach (var item in compositions)
            {
                foreach (var item2 in item)
                {
                    Add(item2);
                }
            }
        }

        public Composition(List<Func<T, T>> functions)
        {
            foreach (var func in functions)
            {
                Add(func);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator<Func<T, T>> IEnumerable<Func<T, T>>.GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        public Composition<T> Add(Func<T, T> func)
        {
            _list.AddLast(func);
            var tmp = _function;
            _function = Compose(func, tmp);
            return this;
        }

        public T Execute(T x)
        {
            return _function(x);
        }

        private static Func<T, T> Compose(Func<T, T> t1, Func<T, T> t2)
        {
            if (t1 == null) return t2;
            if (t2 == null) return t1;
            return x => t1(t2(x));
        }
    }
}