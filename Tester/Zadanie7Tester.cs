﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zadanie7;

namespace Testers
{
    [TestClass]
    public class Zadanie7Tester
    {
        private static readonly string name = "Nazwa.txt";
        private static readonly string content = "ala ma kota i kot ma ale";
        private Memento Create()
        {
            string name = "Nazwa.txt";
            string content = "ala ma kota i kot ma ale";
            return new Memento(name, content);
        }

        private void CheckData(Memento m, string fileName, string fileContext)
        {
            Assert.AreEqual(fileName, m.FileName);
            Assert.AreEqual(fileContext, m.FileContext);
        }
        private void CheckData(Document m, string fileName, string fileContext)
        {
            Assert.AreEqual(fileName, m.FileName);
            Assert.AreEqual(fileContext, m.FileContext);
        }
        private void CheckFont(Memento m, bool expcecteIsBold = false, bool expcecteIsItalic = false, bool expcecteIsUnderlined = false)
        {
            Assert.AreEqual(expcecteIsBold, m.Font.IsBold);
            Assert.AreEqual(expcecteIsItalic, m.Font.IsItalic);
            Assert.AreEqual(expcecteIsUnderlined, m.Font.IsUnderlined);
        }
        private void CheckFont(Document m, bool expcecteIsBold = false, bool expcecteIsItalic = false, bool expcecteIsUnderlined = false)
        {
            Assert.AreEqual(expcecteIsBold, m.IsTextBold);
            Assert.AreEqual(expcecteIsItalic, m.IsTextItalic);
            Assert.AreEqual(expcecteIsUnderlined, m.IsTextUnderlined);
        }

        [TestMethod]
        public void Renaming()
        {
            string newName = "NewNazwa.txt";
            Memento m = Create();
            CheckData(m, name, content);
            m.SetDocumentName(newName);
            CheckData(m, newName, content);
            CheckFont(m);
        }

        [TestMethod]
        public void ChangeContext()
        {
            Memento m = Create();
            string newFileContext = "Ciekawe zadanie do rozwiazania:\n" + "  SEND\n" + "+ MORE\n" + "_________\n" + " MONEY";
            m.SetContext(newFileContext);
            CheckData(m, name, newFileContext);
            CheckFont(m);
        }

        [TestMethod]
        public void ChangeIs()
        {
            Memento m = Create();
            m.ChangeBold();
            m.ChangeUnderline();
            m.ChangeItalic();
            CheckFont(m, true, true, true);
            m.ChangeUnderline();
            m.ChangeItalic();
            CheckFont(m, true);
        }

        [TestMethod]
        public void AddAtributes()
        {
            Memento m = Create();
            Assert.AreEqual(0, m.GetNumberOfAdditionalAttributes());
            Dictionary<string, string> attributes = new Dictionary<string, string>
            {
                {"author", "Marcin"},
                {"group", "Programmers"}
            };
            foreach (var data in attributes)
            {
                m.SetAttribute(data.Key, data.Value);
            }
            foreach (var data in attributes)
            {
                Assert.AreEqual(data.Value, m.GetAttribute(data.Key));
            }
        }

        [TestMethod]
        public void Equality()
        {
            Memento m = Create();
            Memento m2 = Create();
            Assert.AreEqual(m, m2);
            Assert.AreNotSame(m, m2);
            m.ChangeBold();
            Assert.AreNotEqual(m, m2);
            m.ChangeBold();
            Assert.AreEqual(m, m2);
            m.SetContext("Inny kontekst");
            Assert.AreNotEqual(m, m2);

        }
        /********/

        [TestMethod]
        public void Simple()
        {
            Document d = new Document();
            Assert.AreEqual("", d.FileName);
            Assert.AreEqual("", d.FileContext);
            Assert.IsFalse(d.IsTextBold);
            Assert.IsFalse(d.IsTextItalic);
            Assert.IsFalse(d.IsTextUnderlined);
            Assert.AreEqual(0, d.AttributesSize);
        }

        [TestMethod]
        public void Undo()
        {
            List<string> list = new List<string>()
            {
                "Ala ma kota",
                "Ala nie ma psa",
                "Krzysztof ma matke, ma brata"
            };
            Document d = new Document();
            for (int i = 0; i < list.Count; ++i)
            {
                d.SetDocumentContext(list[i]);
                Assert.AreEqual(list[i], d.FileContext);
            }
            for (int i = list.Count - 1; i >= 0; --i)
            {
                Assert.AreEqual(list[i], d.FileContext);
                d.Revert();
            }
        }
        /*****/

        [TestMethod]
        public void NoArgumentsCommands()
        {
            Document d = new Document();
            CommandInvoker invoker = new CommandInvoker(d);
            CheckFont(d);
            invoker.AddCommand(CommandInvoker.CommandType.ChangeBold);
            invoker.AddCommand(CommandInvoker.CommandType.ChangeItalics);
            invoker.AddCommand(CommandInvoker.CommandType.ChangeUnderlining);
            CheckFont(d);
            invoker.Execute();
            CheckFont(d, true, true, true);
        }

        [TestMethod]
        public void OneArgumentCommand()
        {
            string context2 = "To jest jakiś nowy kontekst";
            string name2 = "NowaNazwa.txt";
            Document d = new Document();
            CommandInvoker invoker = new CommandInvoker(d);
            CheckData(d, "", "");
            invoker.AddCommand(CommandInvoker.CommandType.SetDocumentContext, context2);
            invoker.AddCommand(CommandInvoker.CommandType.SetDocumentName, "problem!");
            invoker.AddCommand(CommandInvoker.CommandType.SetDocumentName, name2);
            invoker.Execute();
            CheckData(d, name2, context2);
        }

        [TestMethod]
        public void TwoArgumentsMethod()
        {
            string author = "Marcin";
            string nr = "927413719";
            Document d = new Document();
            CommandInvoker invoker = new CommandInvoker(d);
            invoker.AddCommand(CommandInvoker.CommandType.SetDocumentAttribute, "Author", author);
            invoker.AddCommand(CommandInvoker.CommandType.SetDocumentAttribute, "Number", nr);
            invoker.Execute();
            var data = d.GetAttributes();
            Assert.AreEqual("Author",data[0].Item1);
            Assert.AreEqual(author, data[0].Item2);
            Assert.AreEqual("Number", data[1].Item1);
            Assert.AreEqual("927413719", data[1].Item2);
        }
    }
}
