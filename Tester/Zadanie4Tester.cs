﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zadanie4;
namespace Testers
{
    [TestClass]
    public class Zadanie4Tester
    { 
        [TestMethod]
        public void ComposeOneFunction()
        {
            Func<int, int> linearfunction = x => 2 * x + 10;
            Composition<int> comp = new Composition<int>(linearfunction);
            Assert.AreEqual(10, comp.Execute(0));
            Assert.AreEqual(14, comp.Execute(2));
            Assert.AreEqual(20, comp.Execute(5));
        }
        [TestMethod]
        public void ComposeTwoFunctions()
        {
            Func<int, int> linearfunction = (x) => 2 * x + 5;
            Func<int, int> quadraticfunction = (x) => x * x;

            Composition<int> comp = new Composition<int>(linearfunction, quadraticfunction);
            Assert.AreEqual(25, comp.Execute(0));
            Assert.AreEqual(81, comp.Execute(2));
            Assert.AreEqual(225, comp.Execute(5));

            comp = new Composition<int>(quadraticfunction, linearfunction);
            Assert.AreEqual(5, comp.Execute(0));
            Assert.AreEqual(13, comp.Execute(2));
            Assert.AreEqual(55, comp.Execute(5));

            comp = new Composition<int>(linearfunction, linearfunction);
            Assert.AreEqual(15, comp.Execute(0));
            Assert.AreEqual(23, comp.Execute(2));
            Assert.AreEqual(35, comp.Execute(5));

            comp = new Composition<int>(quadraticfunction, quadraticfunction);
            Assert.AreEqual(0, comp.Execute(0));
            Assert.AreEqual(16, comp.Execute(2));
            Assert.AreEqual(625, comp.Execute(5));
        }
        [TestMethod]
        public void ComposeThreeFunctions()
        {
            Func<int, int> identity = x => x;
            Func<int, int> linearfunction = x => x + 2;
            Func<int, int> cubicfunction = x => x * x * x - 1;
            var functions = new List<Func<int, int>> { identity, linearfunction, cubicfunction };

            Composition<int> compose = new Composition<int>(functions);
            Assert.IsNotNull(compose._function);
            Assert.AreEqual(7, compose.Execute(0));
            Assert.AreEqual(63, compose.Execute(2));
            Assert.AreEqual(26, compose.Execute(1));

        }
        [TestMethod]
        public void AddingFunctionToComposition()
        {
            Func<int, int> identity = x => x;

            Composition<int> comp = new Composition<int>(identity);
            Assert.AreEqual(0, (int)comp.Execute(0));
            Assert.AreEqual(2, (int)comp.Execute(2));

            Func<int, int> linearfunction = x => x + 2;

            comp.Add(linearfunction);
            Assert.AreEqual(2, (int)comp.Execute(0));
            Assert.AreEqual(4, (int)comp.Execute(2));

            Func<int, int> cubicfunction = x => x * x * x;

            comp.Add(cubicfunction);
            Assert.AreEqual(8, (int)comp.Execute(0));
            Assert.AreEqual(64, (int)comp.Execute(2));
        }
        [TestMethod]
        public void CompositionOfCompositions()
        {
            Func<int, int> linearfunction = x => x + 2;
            Func<int, int> quadraticfunction = x => 2 * x * x;
            Composition<int> comp1 = new Composition<int>(linearfunction, quadraticfunction);
            Composition<int> comp2 = new Composition<int>(quadraticfunction, linearfunction);
            Composition<int> compOfComps = new Composition<int>(comp1, comp2);
            Assert.AreEqual(130, (int)compOfComps.Execute(0));
            Assert.AreEqual(650, (int)compOfComps.Execute(1));
            Assert.AreEqual(2050, (int)compOfComps.Execute(2));
        }
        [TestMethod]
        public void CheckIterator()
        {
            Func<int, int> identity = x => x;
            Func<int, int> linearfunction = x => 3 * x - 2;
            Func<int, int> quadraticfunction = x => 2 * x * x - 5;
            Func<int, int> cubicfunction = x => x * x * x + x * x + x + 1;

            List<Func<int, int>> functions = new List<Func<int, int>>
            {
                identity,
                linearfunction,
                quadraticfunction,
                cubicfunction
            };
            Composition<int> comp = new Composition<int>(functions);
            int[] tab0 = { 0, -2, -5, 1 };
            int[] tab1 = { 1, 1, -3, 4 };
            int[] tab2 = { 2, 4, 3, 15 };
            int i = 0;
            foreach (var f in comp)
            {
                Assert.AreEqual(f(0), tab0[i]);
                Assert.AreEqual(f(1), tab1[i]);
                Assert.AreEqual(f(2), tab2[i]);
                i++;
            }
        }

    }
}