﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Zadanie1;
using Zadanie1.Export;
using Zadanie1.Goods;
using Zadanie1.Import;

namespace Testers
{
    [TestClass]
    public class Tests 
    {
        [TestMethod]
        public void TestExporter()
        {
            var textToBeExported = "Ala ma kota";
            Exporter exporter = new TextExporter(textToBeExported);
            var exportedData = exporter.ExportData;
            var exportedText = (exportedData as TextData).Text;
            Assert.AreEqual(textToBeExported, exportedText);
            exportedData = exporter.ExportData;
            exportedText = (exportedData as TextData).Text;
            textToBeExported = string.Empty;
            Assert.AreEqual(textToBeExported, exportedText);
        }

        [TestMethod]
        public void TestImporter()
        {
            var textToBeImported = "Ala zgubila dolara";
            Data dataToSendToImporter = new TextData(textToBeImported);
            Importer importer = new TextImporter();
            importer.ImportData(dataToSendToImporter);
            var dataSavedInImporter = (importer as TextImporter).ImportedText;
            Assert.AreEqual(textToBeImported, dataSavedInImporter);
        }

        [TestMethod]
        public void TestFactory()
        {
            const string textToForFactory = "Ali kot zjadl dolara";
            DistributedModuleFactory factory = new DistributedModuleTextFactory(textToForFactory);
            var dataFromFactory = factory.CreateData();
            var textFromModule = (dataFromFactory as TextData).Text;
            Assert.AreEqual(textToForFactory, textFromModule);
            var exporter = factory.CreateExporter();
            textFromModule = ((exporter as TextExporter).ExportData as TextData).Text;
            Assert.AreEqual(textToForFactory, textFromModule);
            var importer = factory.CreateImporter();
            Assert.IsTrue(importer is TextImporter);
        }
    }
}