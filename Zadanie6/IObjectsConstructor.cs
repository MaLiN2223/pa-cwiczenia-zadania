﻿namespace Zadanie6
{
    public interface IObjectsConstructor
    {
        bool ConstructObjectFromRecipe(ConstructionRecipe obj);
    }
}