﻿namespace Zadanie6
{
    public interface ControllerTemplateMethod
    {
        void Execute();
    }
}