﻿namespace Zadanie6
{
    public abstract class IProductionLineMover
    {
        public abstract bool MoveProductionLine(MovingDirection dir);
    }
}