﻿using System;

namespace Zadanie6
{
    public interface ILogger
    {
        ILogger Log(LoggingType type, bool isAny);
        ILogger Callback(Action act);
        void Log(LoggingType error, string v);
    }
}