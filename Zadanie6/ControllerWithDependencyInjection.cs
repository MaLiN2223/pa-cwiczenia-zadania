﻿using System;

namespace Zadanie6
{
    public class ControllerWithDependencyInjection : ControllerTemplateMethod
    {
        public ControllerWithDependencyInjection(
            IObjectsConstructor objectsConstructor,
            IConstructionRecipeCreator constructionRecipeCreator, ILogger logger,
            IProductionLineMover productionLineMover)
        {
            Constructor = objectsConstructor;
            ConstructionRecipe = constructionRecipeCreator;
            Logger = logger;
            Mover = productionLineMover;
        }

        private IObjectsConstructor Constructor { get; }
        private IConstructionRecipeCreator ConstructionRecipe { get; }
        private ILogger Logger { get; }
        private IProductionLineMover Mover { get; }

        public void Execute()
        {
            try
            {
                Mover.MoveProductionLine(MovingDirection.Forward);
                for (var i = 0; i < ConstructionRecipe.NumberOfElementsToProduce; ++i)
                {
                    if (Constructor.ConstructObjectFromRecipe(ConstructionRecipe.ConstructionRecipe))
                    {
                        Logger.Log(LoggingType.Info, ConstructionRecipe.ConstructionRecipe.NameOfObject);
                        Mover.MoveProductionLine(MovingDirection.Forward);
                    }
                    else
                    {
                        Logger.Log(LoggingType.Warning, ConstructionRecipe.ConstructionRecipe.NameOfObject);
                        Mover.MoveProductionLine(MovingDirection.ToScran);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Log(LoggingType.Error, ConstructionRecipe.ConstructionRecipe.NameOfObject);
            }
        }
    }
}