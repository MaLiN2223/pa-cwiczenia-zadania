﻿namespace Zadanie6
{
    public interface IConstructionRecipeCreator
    {
        ConstructionRecipe ConstructionRecipe { get; set; }
        uint NumberOfElementsToProduce { get; set; }
        object Log(LoggingType error, string seriouserror);
    }
}