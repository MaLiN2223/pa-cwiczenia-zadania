﻿using Zadanie1.Goods;

namespace Zadanie1.Export
{
    public interface Exporter
    {
        Data DataToExport { get; set; }
        Data ExportData { get; }
    }
}