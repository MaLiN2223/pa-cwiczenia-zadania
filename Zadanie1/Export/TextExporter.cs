﻿using Zadanie1.Goods;

namespace Zadanie1.Export
{
    public class TextExporter : Exporter
    {
        public TextExporter(string textToBeExported)
        {
            DataToExport = new TextData(textToBeExported);
        }

        public Data DataToExport { get; set; }

        public Data ExportData
        {
            get
            {
                if (DataExportedOrNotSet())
                    return TextData.GetEmpty();

                var toReturn = DataToExport;
                DataToExport = null;
                return toReturn;
            }
        }

        private bool DataExportedOrNotSet()
        {
            return DataToExport == null;
        }
    }
}