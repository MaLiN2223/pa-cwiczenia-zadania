﻿using Zadanie1.Export;
using Zadanie1.Goods;
using Zadanie1.Import;

namespace Zadanie1
{
    public interface DistributedModuleFactory
    {
        Data CreateData();
        Exporter CreateExporter();
        Importer CreateImporter();
    }
}