﻿namespace Zadanie1.Goods
{
    public class TextData : Data
    {
        public TextData(string textToBeImported)
        {
            Text = textToBeImported;
        }

        public string Text { get; private set; }

        public static TextData GetEmpty()
        {
            return new TextData(string.Empty);
        }
    }
}