﻿using Zadanie1.Export;
using Zadanie1.Goods;
using Zadanie1.Import;

namespace Zadanie1
{
    public class DistributedModuleTextFactory : DistributedModuleFactory
    {
        private readonly string _textForFactoring;

        public DistributedModuleTextFactory(string textForFactoring)
        {
            _textForFactoring = textForFactoring;
        }

        public Exporter CreateExporter()
        {
            return new TextExporter(_textForFactoring);
        }

        public Importer CreateImporter()
        {
            return new TextImporter();
        }

        public Data CreateData()
        {
            return new TextData(_textForFactoring);
        }
    }
}