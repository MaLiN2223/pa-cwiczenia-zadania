﻿using Zadanie1.Goods;

namespace Zadanie1.Import
{
    public interface Importer
    {
        Data ImportedData { get; set; }
        void ImportData(Data dataToSendToImporter);
    }
}