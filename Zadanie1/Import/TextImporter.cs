﻿using Zadanie1.Goods;

namespace Zadanie1.Import
{
    public class TextImporter : Importer
    {
        public string ImportedText
        {
            get
            {
                var textData = ImportedData as TextData;
                return textData != null ? textData.Text : string.Empty;
            }
        }

        public void ImportData(Data dataToSendToImporter)
        {
            ImportedData = dataToSendToImporter;
        }

        public Data ImportedData { get; set; }
    }
}