﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Zadanie5
{
    internal class Pair<T> : IComparable<Pair<T>>, IEquatable<Pair<T>> where T : IComparable<T>
    {
        public Pair(T t)
        {
            First = t;
            Second = 1;
        }

        internal Pair()
        {
            Second = -1;
        }

        public T First { get; set; }
        public int Second { get; set; }

        public int CompareTo(Pair<T> other)
        {
            if ((other.Second == Second) && Second == -1)
                return 0;
            if (other.Second == -1)
                return -1;
            if (Second == -1)
                return 1;
            return First.CompareTo(other.First);
        }

        public bool Equals(Pair<T> other)
        {
            return ((other.Second == Second) && Second == -1) || First.CompareTo(other.First) == 0;
        }
    }

    public class SortedDeque<T> : IEnumerable<T> where T : IComparable<T>
    {
        private readonly int _bucketSize;
        private int lastEmpty;
        private readonly List<Pair<T>> list = new List<Pair<T>>();

        public SortedDeque(int size, int bucketSize)
        {
            MaxSize = size;
            _bucketSize = bucketSize;
            lastEmpty = 0;
            Size = 0;
        }

        public int UniqueSize { get; private set; }
        public int Size { get; private set; }
        public int MaxSize { get; }

        public bool Empty => list.Count == 0;

        private T this[int i]
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public T Front
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var item in list)
            {
                for (var i = 0; i < item.Second; ++i)
                {
                    yield return item.First;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private void AddMemory()
        {
            if (lastEmpty + _bucketSize > MaxSize)
                throw new InsufficientMemoryException("Cant create bucket");
            for (var i = 0; i < _bucketSize; ++i)
            {
                list.Add(new Pair<T>());
            }
        }

        public void Push(T t)
        {
            if (Empty)
            {
                AddMemory();
            }
            if (lastEmpty >= MaxSize)
                throw new InsufficientMemoryException("Deque is full");
            if (lastEmpty == list.Count)
                AddMemory();
            var toPut = new Pair<T>(t);
            Size++;
            foreach (var item in list)
            {
                if (!item.Equals(toPut)) continue;
                item.Second++;
                return;
            }
            list[lastEmpty++] = toPut;
            list.Sort();
            UniqueSize++;
        }

        public T PopFront()
        {
            if (Empty)
                throw new InvalidOperationException(@"Deque is empty!");
            var output = list[0].First;
            if (list[0].Second > 1)
            {
                list[0].Second--;
            }
            else
            {
                list.RemoveAt(0);
                list.Add(new Pair<T>());
                UniqueSize--;
            }
            Size--;
            return output;
        }

        public T PopBack()
        {
            if (Empty)
                throw new InvalidOperationException(@"Deque is empty!");
            var output = list[lastEmpty - 1].First;

            if (list[lastEmpty - 1].Second > 1)
                list[lastEmpty - 1].Second--;
            else
            {
                list.RemoveAt(lastEmpty - 1);
                list.Add(new Pair<T>());
                UniqueSize--;
                lastEmpty--;
            }
            Size--;
            return output;
        }

        public StringBuilder ToBuilder()
        {
            var builder = new StringBuilder(Size);
            foreach (var v in list)
            {
                builder.AppendLine($"{v.First}, {v.Second}");
            }
            return builder;
        }
    }
}