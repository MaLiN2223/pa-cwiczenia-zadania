﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zadanie7
{
    public class Document
    {
        private readonly PseudoStack _mementos = new PseudoStack();

        private int _memCount;

        public string FileName { get; set; } = "";
        public string FileContext { get; set; } = "";
        public bool IsTextBold { get; private set; }
        public bool IsTextUnderlined { get; private set; }
        public bool IsTextItalic { get; private set; }

        public int AttributesSize => Attributes.Count;
        internal Dictionary<string, string> Attributes { get; set; } = new Dictionary<string, string>();

        public List<Tuple<string, string>> GetAttributes()
        {
            return Attributes.Select(data => new Tuple<string, string>(data.Key, data.Value)).ToList();
        }

        public void AddAttribute(string key, string val)
        {
            Attributes.Add(key, val);
        }

        public void SetDocumentContext(string context)
        {
            SetMemo();
            FileContext = context;
        }

        public void ChangeBold()
        {
            SetMemo();
            IsTextBold = !IsTextBold;
        }

        public void ChangeItalic()
        {
            SetMemo();
            IsTextItalic = !IsTextItalic;
        }

        public void ChangeUnderline()
        {
            SetMemo();
            IsTextUnderlined = !IsTextUnderlined;
        }

        public void SetDocumentName(string name)
        {
            FileName = name;
        }


        public void Revert()
        {
            if (_memCount == 0)
                return;
            var myMemento = _mementos.Pop();
            FileContext = myMemento.FileContext;
            _memCount--;
        }

        private void SetMemo()
        {
            _mementos.Push(new Memento(this));
            _memCount++;
        }
    }
}