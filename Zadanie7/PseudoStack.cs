﻿using System;
using System.Collections.Generic;

namespace Zadanie7
{
    internal class PseudoStack
    {
        private readonly List<Memento> list = new List<Memento>(30);

        public int Count
        {
            get { return list.Count; }
        }

        public Memento Pop()
        {
            if (list != null)
            {
                var place = list.Count - 1;
                var item = list[place];
                list.RemoveAt(place);
                return item;
            }

            throw new InvalidOperationException();
        }

        public void Push(Memento item)
        {
            if (list.Count >= 30)
                list.RemoveAt(0);
            list.Add(item);
        }
    }
}