﻿namespace Zadanie7
{
    public class FontClass
    {
        public bool IsBold { get; set; } = false;
        public bool IsItalic { get; set; } = false;
        public bool IsUnderlined { get; set; } = false;
        // Pozwalam sobie nie nadpisywać GetHashCode ponieważ jest to niepotrzebne w zadaniu lecz normalnie starał bym się to zrobić
        public override bool Equals(object obj)
        {
            var other = obj as FontClass;
            return other != null && IsBold == other.IsBold && IsItalic == other.IsItalic &&
                   IsUnderlined == other.IsUnderlined;
            ;
        }
    }
}