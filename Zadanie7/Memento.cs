﻿using System.Collections.Generic;
using System.Linq;

namespace Zadanie7
{
    public class Memento
    {
        public Memento(string fileName, string fileContext)
        {
            FileName = fileName;
            FileContext = fileContext;
            Font = new FontClass();
        }

        internal Memento(Document d)
        {
            FileName = d.FileName;
            FileContext = d.FileContext;
            Font = new FontClass
            {
                IsBold = d.IsTextBold,
                IsUnderlined = d.IsTextUnderlined,
                IsItalic = d.IsTextItalic
            };
            Attributes = d.Attributes;
        }
        public string FileContext { get; private set; }
        public FontClass Font { get; }
        public string FileName { get; private set; }

        public int GetNumberOfAdditionalAttributes()
        {
            return Attributes.Count;
        }

        public void SetDocumentName(string newName)
        {
            FileName = newName;
        }

        public void SetContext(string newFileContext)
        {
            FileContext = newFileContext;
        }

        public void ChangeBold()
        {
            Font.IsBold = !Font.IsBold;
        }

        public void ChangeItalic()
        {
            Font.IsItalic = !Font.IsItalic;
        }

        public void ChangeUnderline()
        {
            Font.IsUnderlined = !Font.IsUnderlined;
        }

        public void SetAttribute(string key, string data)
        {
            Attributes.Add(key, data);
        }

        public string GetAttribute(string key)
        {
            return Attributes[key];
        }

        // Pozwalam sobie nie nadpisywać GetHashCode ponieważ jest to niepotrzebne w zadaniu lecz normalnie starał bym się to zrobić

        public override bool Equals(object obj)
        {
            var other = obj as Memento;
            return other != null &&
                   other.FileName.Equals(FileName) &&
                   other.Font.Equals(Font) &&
                   other.FileContext.Equals(FileContext)
                   && other.GetNumberOfAdditionalAttributes() == GetNumberOfAdditionalAttributes() &&
                   other.Attributes.SequenceEqual(Attributes);
        }

        private Dictionary<string, string> Attributes { get; } = new Dictionary<string, string>();
    }
}