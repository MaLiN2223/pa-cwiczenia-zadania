﻿using System;
using System.Collections.Generic;

namespace Zadanie7
{
    public class CommandInvoker
    {
        public enum CommandType
        {
            SetDocumentContext = -1,
            SetDocumentName = -2,
            ChangeBold = 1,
            ChangeItalics = 2,
            ChangeUnderlining = 3,
            SetDocumentAttribute = 0
        }

        private readonly Document _document;
        private readonly List<Action> list = new List<Action>();

        public CommandInvoker(Document dataContext)
        {
            _document = dataContext;
        }

        public void AddCommand(CommandType type, string arg)
        {
            if (type >= 0)
                throw new NotSupportedException();
            list.Add(GoString(type, arg));
        }

        public void AddCommand(CommandType type)
        {
            if (type <= 0)
                throw new NotSupportedException();
            list.Add(GoBool(type));
        }

        public void AddCommand(CommandType type, string key, string val)
        {
            if (type != 0)
                throw new NotSupportedException();
            list.Add(GoDocument(key, val));
        }

        private Action GoString(CommandType type, string arg)
        {
            if (type == CommandType.SetDocumentContext)
            {
                return () => { _document.SetDocumentContext(arg); };
            }
            return () => { _document.SetDocumentName(arg); };
        }

        private Action GoBool(CommandType type)
        {
            if (type == CommandType.ChangeBold)
                return () => { _document.ChangeBold(); };
            if (type == CommandType.ChangeItalics)
                return () => { _document.ChangeItalic(); };
            return () => { _document.ChangeUnderline(); };
        }

        private Action GoDocument(string key, string value)
        {
            return () => { _document.AddAttribute(key, value); };
        }

        public void Execute()
        {
            for (var i = 0; i < list.Count; ++i)
            {
                list[i].Invoke();
            }
            list.Clear();
        }
    }
}