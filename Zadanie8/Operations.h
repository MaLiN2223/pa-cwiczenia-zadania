#pragma once
class Operations
{
public:
	template<int N>  void BubbleSort(int * data) {
		loop<N - 1>(data);
		BubbleSort<N - 1>(data);
	}
	template<int N> int Fact() {
		return Factorial<N>::val;
	}
	template<int N> int Fibb()
	{
		return Fibonacci<N>::val;
	}
	template<int N> double getPi()
	{
		return 4 * Pi<N>();
	}
	template<int N, int K> long getNewton()
	{
		return Newton<N, K>::val;
	}
	template<int N, int K> int getGCD()
	{
		return GCD<N, K>();
	}
private:
	template<int N, int K>
	struct maxInt
	{
		const static int val = N > K ? N : K;
	};
	template<int N, int K>
	struct minInt
	{
		const static int val = N < K ? N : K;
	};
	template<int N, int K>
	struct GCD
	{
		const static int val = GCD < minInt<N, K>::val, maxInt<N, K>::val%minInt<N, K>::val>::val;
	};
	template<int N>
	struct GCD<N, 0>
	{
		const static int val = N;
	};

	template<int N, int K>
	struct Newton
	{
		const static long val = Newton<N - 1, K - 1>::val + Newton<N - 1, K>::val;
	};
	template<>
	struct Newton<0, 0>
	{
		const static long val = 1;
	};
	template<int N>
	struct Newton<N, 0>
	{
		const static long val = 1;
	};
	template<int N>
	struct Newton<N, N>
	{
		const static long val = 1;
	};

	template <int N>
	double Pi()
	{
		double a = 4.0 * N*(N + 1.0);
		a = (a*1.0) / (a + 1.0);
		return a * Pi<N - 1>()*1.0;
	};
	template<>
	double Pi<1>() {
		return 8.0 / 9.0;
	}
	template<int N> struct Fibonacci {
		enum { val = Fibonacci<N - 1>::val + Fibonacci<N - 2>::val };
	};
	template<int N> struct Factorial
	{
		enum { val = Factorial<N - 1>::val *N };
	};
	template<int N> void loop(int *data) {
		if (data[0] > data[1]) std::swap(data[0], data[1]);
		loop<N - 1>(++data);
	}
	template<>  inline void BubbleSort<2>(int * data) {
		loop<1>(data);
	};
	template<> inline void loop<0>(int *data) {};
	template<>struct Factorial<1>
	{
		enum { val = 1 };
	};
	template<>struct Factorial<0>
	{
		enum { val = 1 };
	};
	template<> struct Fibonacci<1> {
		enum { val = 1 };
	};
	template<> struct Fibonacci<2> {
		enum { val = 1 };
	};
};