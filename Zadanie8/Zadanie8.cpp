// Zadanie8.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <gtest/gtest.h> 
#include <algorithm>
#include "Operations.h"
using namespace std;
using namespace ::testing;
struct Tester : public ::testing::Test
{
	Operations operations = Operations();
};
TEST_F(Tester, sortowanie)
{
	int data[10] = { 4,6,7,5,3,1,4,8,9,1 };
	int sorted[10] = { 4,6,7,5,3,1,4,8,9,1 };
	sort(begin(sorted), end(sorted));
	operations.BubbleSort<10>(data);
	for (int i = 0;i < 10;++i)
	{
		ASSERT_EQ(sorted[i], data[i]);
	}
}
TEST_F(Tester, silnia)
{
	int god[9] = { 1,1,2,6,24,120,720,5040,40320 };
	ASSERT_EQ(god[0], operations.Fact<0>());
	ASSERT_EQ(god[1], operations.Fact<1>());
	ASSERT_EQ(god[2], operations.Fact<2>());
	ASSERT_EQ(god[3], operations.Fact<3>());
	ASSERT_EQ(god[4], operations.Fact<4>());
	ASSERT_EQ(god[5], operations.Fact<5>());
	ASSERT_EQ(god[6], operations.Fact<6>());
	ASSERT_EQ(god[7], operations.Fact<7>());
	ASSERT_EQ(god[8], operations.Fact<8>());
}
TEST_F(Tester, Fibonacci)
{
	int god[9] = { 1,2,3,5,8,13,21,34,55 };
	ASSERT_EQ(god[0], operations.Fibb<2>());
	ASSERT_EQ(god[1], operations.Fibb<3>());
	ASSERT_EQ(god[2], operations.Fibb<4>());
	ASSERT_EQ(god[3], operations.Fibb<5>());
	ASSERT_EQ(god[4], operations.Fibb<6>());
	ASSERT_EQ(god[5], operations.Fibb<7>());
	ASSERT_EQ(god[6], operations.Fibb<8>());
	ASSERT_EQ(god[7], operations.Fibb<9>());
	ASSERT_EQ(god[8], operations.Fibb<10>());
}
TEST_F(Tester, Newton)
{
	long tmp = operations.getNewton<20, 15>();
	ASSERT_EQ(15504, tmp);
	tmp = operations.getNewton<20, 1>();
	ASSERT_EQ(20, tmp);
	tmp = operations.getNewton<30, 1>();
	ASSERT_EQ(30, tmp);
	tmp = operations.getNewton<15, 13>();
	ASSERT_EQ(105, tmp);
	tmp = operations.getNewton<25, 13>();
	ASSERT_EQ(5200300, tmp);
}
TEST_F(Tester, GCD)
{
	int tmp = operations.getNewton<20, 15>();
	ASSERT_EQ(5, tmp);
	tmp = operations.getNewton<0, 15>();
	ASSERT_EQ(1, tmp);
	tmp = operations.getNewton<100, 3>();
	ASSERT_EQ(1, tmp);
	tmp = operations.getNewton<42, 56>();
	ASSERT_EQ(14, tmp);
	tmp = operations.getNewton<24, 54>();
	ASSERT_EQ(6, tmp);

}
double pi(int n)
{
	if (n == 1)
		return 8.0 / 9.0;
	double a = 4.0 * n*(n + 1.0);
	a = (a*1.0) / (a + 1.0);
	return a * pi(n - 1)*1.0;
}
TEST_F(Tester, Pi)
{
	double data[10];
	for (int i = 1;i < 10;++i)
	{
		data[i] = 4 * pi(i);
	}

	ASSERT_EQ(data[1], operations.getPi<1>());
	ASSERT_EQ(data[2], operations.getPi<2>());
	ASSERT_EQ(data[3], operations.getPi<3>());
	ASSERT_EQ(data[4], operations.getPi<4>());
	ASSERT_EQ(data[5], operations.getPi<5>());
	ASSERT_EQ(data[6], operations.getPi<6>());
	ASSERT_EQ(data[7], operations.getPi<7>());
	ASSERT_EQ(data[8], operations.getPi<8>());
}
int main(int argc, char* argv[])
{
	InitGoogleTest(&argc, argv);
	int retVal = RUN_ALL_TESTS();
	return retVal;
}

