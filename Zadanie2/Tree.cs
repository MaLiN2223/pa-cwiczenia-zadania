﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Zadanie2
{
    public class Tree<T> : IEnumerable<T>
    {
        private EnumeratorOrder _order;

        public Tree(T value, EnumeratorOrder order) :
            this(value, order, new CustomList<Tree<T>>())
        {
        }

        private Tree(T value, EnumeratorOrder order, IEnumerable children)
        {
            var k = children as CustomList<Tree<T>>;
            Children = k ?? new CustomList<Tree<T>>();
            Value = value;
            Order = order;
        }

        public CustomList<Tree<T>> Children { get; }

        public EnumeratorOrder Order
        {
            get { return _order; }
            set
            {
                if (value != EnumeratorOrder.BreadthFirstSearch && value != EnumeratorOrder.DepthFirstSearch)
                    throw new ArgumentOutOfRangeException();
                _order = value;
                SetOrder(this, value);
            }
        }

        public T Value { get; }

        public IEnumerator<T> GetEnumerator()
        {
            return Order == EnumeratorOrder.BreadthFirstSearch ? BFS(this).GetEnumerator() : DFS(this).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(Tree<T> subtree)
        {
            subtree.Order = Order;
            Children.Add(subtree);
        }

        public void Add(T v)
        {
            Add(new Tree<T>(v, Order));
        }

        private Queue<T> DFS(Tree<T> tree)
        {
            var stack = new Stack<Tree<T>>();
            var queueT = new Queue<T>();
            stack.Push(tree);
            while (stack.Any())
            {
                var current = stack.Pop();
                if (current == null) continue;
                queueT.Enqueue(current.Value);
                foreach (var item in current.Children)
                    Merge(ref queueT, DFS(item));
            }
            return queueT;
        }

        private Queue<T> BFS(Tree<T> tree)
        {
            var queue = new Queue<Tree<T>>();
            var queueT = new Queue<T>();
            queue.Enqueue(tree);
            while (queue.Count > 0)
            {
                var current = queue.Dequeue();
                if (current == null) continue;
                queueT.Enqueue(current.Value);
                foreach (var item in current.Children)
                    queue.Enqueue(item);
            }
            return queueT;
        }

        private static void SetOrder(Tree<T> tree, EnumeratorOrder order)
        {
            foreach (var item in tree.Children)
            {
                item.Order = order;
            }
        }

        private static void Merge(ref Queue<T> q1, Queue<T> q2)
        {
            foreach (var item in q2)
                q1.Enqueue(item);
        }
    }
}