﻿namespace Zadanie2
{
    public enum EnumeratorOrder
    {
        BreadthFirstSearch = 1,
        DepthFirstSearch = 2
    }
}