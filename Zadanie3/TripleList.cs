﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Zadanie3
{
    public class TripleList<T> : IEnumerable<T>
    {
        public bool Empty = true;
        public T Value { get; set; }
        public TripleList<T> NextElement { get; set; }
        public TripleList<T> PreviousElement { get; set; }
        public TripleList<T> MiddleElement { get; set; }

        public IEnumerator<T> GetEnumerator()
        {
            yield return Value;
            if (MiddleElement != null)
            {
                yield return MiddleElement.Value;
            }
            if (NextElement != null)
            {
                var it = NextElement.GetEnumerator();
                while (it.MoveNext())
                {
                    yield return it.Current;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public int Count()
        {
            var output = 0;
            if (!Empty)
                output++;
            if (MiddleElement != null)
                output++;
            if (NextElement != null)
                output += NextElement.Count();
            return output;
        }

        public void Add(T value)
        {
            if (Empty)
            {
                Empty = false;
                Value = value;
            }
            else if (MiddleElement == null)
            {
                MiddleElement = new TripleList<T> {value};
                MiddleElement.MiddleElement = this;
                MiddleElement.Empty = false;
            }
            else if (NextElement == null)
            {
                NextElement = new TripleList<T> {value};
                NextElement.PreviousElement = this;
                NextElement.Empty = false;
            }
            else
            {
                NextElement.Add(value);
            }
        }

        public void Add(TripleList<T> value)
        {
            foreach (var item in value)
            {
                Add(item);
            }
        }
    }
}